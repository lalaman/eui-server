const Project = require('../../models/Project');
const Media = require('../../models/Media');
const { uploadFile, deleteFile } = require('../aws/s3');
const { generateThumb } = require('../../utils/image');

/**
 * Get all projects
 * @param {object} opts - { withGraphFetched: [], where: {} }
 * @param {object} trx
 */
const getAll = async (opts, trx) => {
  const query = Project.query(trx);
  if (opts?.withGraphFetched?.length)
    query.withGraphFetched(`[${opts.withGraphFetched.join(', ')}]`);
  if (opts?.where) query.where(opts.where);

  const project = await query;
  return project;
};

/**
 * Get project by id
 * @arg {number} projectId - id of project
 * @arg {object} opts - { withGraphFetched: [], where: {} }
 * @arg {object} trx
 */
const getById = async (projectId, opts, trx) => {
  const query = Project.query(trx);
  if (opts?.withGraphFetched?.length)
    query.withGraphFetched(`[${opts.withGraphFetched.join(', ')}]`);
  if (opts?.where) query.where(opts.where);

  const project = await query.findById(projectId);
  return project;
};

/**
 * Create project
 * @arg {string} arg1.name - name of project
 * @arg {number} arg1.developer_id - id of models/Developer
 * @arg {object} arg1.address - { street, street_alt, city, state, country, zip }
 * @arg {object} trx
 * @returns {object} - models/Project
 */
const create = async ({ name, developer_id, address }, trx) => {
  const project = await Project.query(trx).insertAndFetch({
    name,
    developer_id,
    address
  });

  return project;
};

/**
 * Update project
 * @arg {string} arg1.name - name of project
 * @arg {number} arg1.developer_id - id of models/Developer
 * @arg {object} arg1.address - { street, street_alt, city, state, country, zip }
 * @arg {object} trx
 * @returns {object} - models/Project
 */
const update = async (projectId, { name, developer_id, address }, trx) => {
  const project = await Project.query(trx).patchAndFetchById(projectId, {
    name,
    developer_id,
    address
  });

  return project;
};

/**
 * Get project renderings
 * @arg {number} projectId - id of project
 * @arg {object} trx
 * @returns - { hero_image: {}, renderings: []}
 */
const getRenderings = async (projectId, trx) => {
  const params = {
    project_id: projectId,
    entity_name: 'project',
    entity_id: projectId
  };

  const heroImage = await Media.query(trx).findOne({
    ...params,
    type: 'hero_image'
  });

  const renderings = await Media.query(trx).where({
    ...params,
    type: 'rendering'
  });

  const data = {
    hero_image: heroImage || null,
    renderings
  };

  return data;
};

/**
 * Upload project renderings to s3 and store in media table
 * @arg {number} projectId
 * @arg {string} type - optional [i.e. 'hero', 'rendering']
 * @arg {array} renderings - [{ tempFilePath, name }]
 * @arg {object} trx
 * @returns {object} - { hero_image, renderings: [] }
 */
const uploadRenderings = async (projectId, type, renderings, trx) => {
  const toDelete = [];
  for (const rendering of renderings) {
    // Upload new rendering
    const key = await uploadFile(projectId, rendering, {
      type: 'renderings',
      secure: false
    });
    // Generate thumbnail
    const thumbBuffer = await generateThumb(rendering.tempFilePath);
    const thumbKey = await uploadFile(
      projectId,
      { name: 'thumb.jpg', file: thumbBuffer },
      { type: 'renderings', secure: false }
    );

    if (type === 'hero') {
      // Check if exists
      const heroImage = await Media.query(trx).findOne({
        project_id: projectId,
        type: 'hero_image'
      });

      // If exists, delete
      if (heroImage) {
        toDelete.push(heroImage.url);
        if (heroImage.thumbnail) toDelete.push(heroImage.thumbnail);
        await Media.query(trx)
          .patch({ url: key, thumbnail: thumbKey })
          .where({ id: heroImage.id });
        continue;
      }
    }

    // Create new media
    await Media.query(trx).insert({
      project_id: projectId,
      entity_id: projectId,
      entity_name: 'project',
      type: type === 'hero' ? 'hero_image' : type,
      url: key,
      thumbnail: thumbKey
    });
  }
  if (toDelete.length) toDelete.map(key => deleteFile(key));
};

/**
 * Remove renderings from s3 and media
 * @arg {number} projectId - id of project
 * @arg {array} mediaIds - list of media ids
 * @arg {object} trx
 */
const removeRenderings = async (projectId, mediaIds, trx) => {
  if (mediaIds?.length) {
    const allMedia = await Media.query(trx)
      .where({ project_id: projectId })
      .whereIn('id', mediaIds);

    if (allMedia.length) {
      await Media.query(trx)
        .delete()
        .where({ project_id: projectId })
        .whereIn('id', mediaIds);

      await Promise.all(
        allMedia.map(m => {
          if (m.url) deleteFile(m.url);
          if (m.thumbnail) deleteFile(m.thumbnail);
        })
      );
    }
  }
};

module.exports = {
  getAll,
  getById,
  create,
  update,
  getRenderings,
  uploadRenderings,
  removeRenderings
};
