const Knex = require('knex');
const { Model } = require('objection');
const pg = require('pg');

const PG_DECIMAL_OID = 1700;
pg.types.setTypeParser(PG_DECIMAL_OID, parseFloat);

const knex = Knex({
  useNullAsDefault: true,
  client: 'postgres',
  connection: {
    host: process.env.DB_HOST || 'localhost',
    port: process.env.DB_PORT || 5432,
    user: process.env.DB_USER,
    password: process.env.DB_PASS,
    database: process.env.DB_NAME
  },
  pool: {
    min: 2,
    max: 1000,
    idleTimeoutMillis: 500
  }
});

// knex.on('query', queryData => {
//   if (process.env.TEST_ENV !== 'ci') {
//     console.log('\x1b[32m%s\x1b[0m', queryData.sql);
//   }
// });

Model.knex(knex);

module.exports = knex;
