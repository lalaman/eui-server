const Media = require('../models/Media');

/**
 * Get media by ids
 * @arg {number} projectId - media project id
 * @arg {array} ids - array of media ids
 * @arg {object} trx
 */
const getByIds = async (projectId, ids = [], trx) => {
  if (ids?.length) {
    const query = Media.query(trx).whereIn('id', ids);
    if (projectId) query.where({ project_id: projectId });

    const media = await query;
    return media;
  }
  return [];
};

module.exports = {
  getByIds
};
