const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const { getById: getAccountById } = require('../services/account');
const Response = require('../utils/response');

const Auth = {
  /**
   * Returns whether token from client is valid
   * @arg {string} req.headers.authorization - token from client
   * @returns {boolean} - true/false
   */
  isAuthenticated: async (req, res, next) => {
    let token;
    try {
      token = req.headers.authorization
        .replace('Bearer ', '')
        .replace('Bearer: ', '');
      if (!token) return Response.unauthorized(res);
    } catch (err) {
      return Response.unauthorized(res);
    }

    // Check for token validity
    try {
      const valid = jwt.verify(token, process.env.JWT_SECRET);
      if (!valid) return Response.unauthorized(res);
    } catch (err) {
      return Response.unauthorized(
        res,
        'Your token is either invalid or has expired.'
      );
    }

    // Check for token expiry
    const { exp, id } = jwt.decode(token, process.env.JWT_SECRET);
    if (exp < Math.floor(new Date() / 1000)) {
      return Response.unauthorized(res, 'Your token has expired.');
    }

    const account = await getAccountById(id);

    if (!account) {
      res.clearCookie('eui_token');
      return Response.unauthorized(res);
    }

    req.account = account;
    return next();
  },

  /**
   * Checks if a unhashed password matches its hash
   * @arg {string} postedPassword - password supplied from client
   * @arg {string} realPassword - hashed password from db
   * @returns {boolean} true/false
   */
  validPassword: (postedPassword, realPassword) =>
    bcrypt.compareSync(postedPassword, realPassword),

  /**
   * Generates token from account data
   * @arg {object} account - models/Account
   * @arg {boolean} keepSignedIn - whether token lasts for shorter or longer period
   * @returns {string} token string
   */
  generateAccountToken: (account, keepSignedIn) => {
    const exp = keepSignedIn
      ? Math.floor(Date.now() / 1000) + 3600 * 24 * 30 // 1 month
      : Math.floor(Date.now() / 1000) + 3600 * 24 * 7; // 1 week

    return account
      ? jwt.sign(
          { exp, id: account.id, contact: account.contact },
          process.env.JWT_SECRET
        )
      : false;
  },

  /**
   * Decodes token if valid
   * @param {string} token
   * @returns {boolean|object} - token data
   */
  decodeToken: token => {
    try {
      const valid = jwt.verify(token, process.env.JWT_SECRET);
      if (!valid) {
        return false;
      }
      // Check for token expiry
      const tokenData = jwt.decode(token, process.env.JWT_SECRET);
      if (tokenData.exp < Math.floor(new Date() / 1000)) {
        return false;
      }
      return tokenData;
    } catch (err) {
      return false;
    }
  }
};

module.exports = Auth;
