const { randomUUID } = require('crypto');
const fs = require('fs');
const MimeTypes = require('mime-types');
const { s3 } = require('./index');

/**
 * Upload file to S3
 * @arg {number} projectId - project id
 * @arg {object} file - { name, file, tempFilePath, file (optional) - buffer }
 * @arg {string} param2.type - 'renderings'
 * @arg {boolean} param2.secure - whether the file can be publically seen without aws signing
 * @arg {string} param2.filepath - filepath in s3
 * @returns {string} s3 key
 */
const uploadFile = async (
  projectId,
  file,
  { type, secure = true, filepath } = {}
) => {
  // Generate hashed filename
  const ext = file.name.split('.').pop().toLowerCase();
  const filename = `${randomUUID()}.${ext}`;

  const url = (() => {
    if (filepath) return filepath;
    return `projects/${projectId}/${type || 'media'}/${filename}`;
  })();

  const Body = file.file || fs.readFileSync(file.tempFilePath);
  const params = {
    Bucket: s3.params.Bucket,
    Key: url,
    Body,
    ACL: !secure ? 'public-read' : undefined,
    ContentType: MimeTypes.lookup(ext),
    ContentDisposition: 'inline'
  };
  await s3.instance.putObject(params).promise();
  return encodeURI(params.Key);
};

/**
 * Delete file from s3
 * @arg {string} key - s3 file uri
 */
const deleteFile = async key => {
  try {
    if (key) {
      const params = {
        Bucket: s3.params.Bucket,
        Key: key
      };
      await s3.instance.deleteObject(params).promise();
    }
  } catch (err) {
    console.log('Unable to delete s3 file', key);
  }
};

/**
 * Sign s3 url
 * @arg {string} url -
 * @arg {number} param1.expiry - expiry time. default 1 day
 * @arg {string} param1.name - name of file (without extension)
 * @returns {string} - signed S3 url
 */
const signUrl = (url, { expiry = 86400, name } = {}) => {
  const ext = name ? url.split('.').pop() : '';
  return s3.instance.getSignedUrl('getObject', {
    Bucket: s3.params.Bucket,
    Key: url,
    Expires: expiry || 86400, // Default to 1 day,
    ...(name
      ? {
          ResponseContentDisposition: `attachment; filename ="${name}.${ext}"`
        }
      : {})
  });
};

module.exports = {
  uploadFile,
  deleteFile,
  signUrl
};
