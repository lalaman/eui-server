const AWS = require('aws-sdk');

AWS.config.update({
  accessKeyId: process.env.AWS_ACCESS_KEY,
  secretAccessKey: process.env.AWS_SECRET_KEY
});

const instance = new AWS.S3({
  region: 'ca-central-1'
});

const s3 = {
  instance,
  params: {
    Bucket: process.env.AWS_S3_BUCKET
  }
};

// const ses = new AWS.SES({
//   apiVersion: '2010-12-01',
//   region: 'us-east-1'
// });

// const lambda = new AWS.Lambda({ region: 'ca-central-1' });

module.exports = {
  s3
  // ses,
  // lambda
};
