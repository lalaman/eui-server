const Developer = require('../models/Developer');
const Project = require('../models/Project');

/**
 * Get all developers
 * @param {object} opts - { withGraphFetched: [], where: {} }
 * @param {object} trx
 */
const getAll = async (opts, trx) => {
  const query = Developer.query(trx);
  if (opts?.withGraphFetched?.length)
    query.withGraphFetched(`[${opts.withGraphFetched.join(', ')}]`);
  if (opts?.where) query.where(opts.where);

  const developers = await query.select([
    Developer.ref('*'),
    Developer.relatedQuery('projects').count().as('projects_count')
  ]);

  developers.forEach(d => {
    d.projects_count = parseInt(d.projects_count, 10);
  });
  return developers;
};

/**
 * Get developer by id
 * @arg {number} developerId - id of developer
 * @arg {object} opts - { withGraphFetched: [], where: [] }
 * @arg {object} trx
 */
const getById = async (developerId, opts, trx) => {
  const query = Developer.query(trx);
  if (opts?.withGraphFetched?.length)
    query.withGraphFetched(`[${opts.withGraphFetched.join(', ')}]`);
  if (opts?.where?.length) query.where(opts.where);

  const developer = await query.findById(developerId);
  return developer;
};

/**
 * Create developer
 * @arg {string} arg1.name - name of developer
 * @arg {object} arg1.address - { street, street_alt, city, state, country, zip }
 * @arg {object} trx
 * @returns {object} - models/Developer
 */
const create = async ({ name, address }, trx) => {
  const developer = await Developer.query(trx).insertAndFetch({
    name,
    address
  });

  return developer;
};

/**
 * Update developer
 * @arg {number} developerId - developer id
 * @arg {string} arg1.name - developer name
 * @arg {object} arg1.address - { street, street_alt, city, state, country, zip }
 * @arg {object} trx
 * @returns {object} - models/Developer
 */
const update = async (developerId, { name, address }, trx) => {
  const developer = await Developer.query(trx)
    .patchAndFetchById(developerId, {
      name, address
    });
  return developer;
};

/**
 * Delete developer
 * @param {number} developerId - developer id
 * @param {object} trx
 * @returns {number} - number of rows deleted
 */
const remove = async (developerId, trx) => {
  // Set all associated projects to null first
  await Project.query(trx)
    .patch({ developer_id: null })
    .where({ developer_id: developerId });

  const deleted = await Developer.query(trx)
    .deleteById(developerId);

  return deleted;
};

module.exports = {
  getAll,
  getById,
  create,
  update,
  remove
};
