const Account = require('../models/Account');
const { hashPassword } = require('../utils/encryption');

/**
 * Get account by email address
 * @arg {string} email - email address
 * @returns {object} models/Account
 */
const getByEmail = async email => {
  const account = await Account.query()
    .findOne({ email: email ? email.toLowerCase() : '' });

  return account;
};

/**
 * Get account by id
 * @param {number} id - account id
 * @param {objects} opts - { withGraphFetched: [] }
 * @param {object} trx
 * @returns {object} models/Account
 */
const getById = async (id, opts, trx) => {
  const query = Account.query(trx).findById(id);
  if (opts?.withGraphFetched?.length)
    query.withGraphFetched(`[${opts.withGraphFetched.join(', ')}]`);

  const account = await query;
  return account;
};

/**
 * Create account from email and password
 * @arg {string} email
 * @arg {string} password - unhashed password
 * @arg {object} trx
 * @returns {object} models/Account
 */
const create = async ({ email, password }, trx) => {
  const hashedPassword = hashPassword(password);
  const account = await Account.query(trx).insertAndFetch({
    email,
    password: hashedPassword
  });
  return account;
};

/**
 * Activate account
 * @arg {number} accountId - id of account to activate
 */
const activate = async accountId => {
  await Account.query().patch({ status: 'active' }).where({ id: accountId });
};

/**
 * Reset login count
 * @arg {number} accountId - id to reset login count
 * @returns {boolean} true/false
 */
const resetLoginCount = async (accountId, trx) => {
  if (!accountId) return false;
  await Account.query(trx)
    .patch({ failed_logins: 0 })
    .where({ id: accountId });
  return true;
};

module.exports = {
  getByEmail,
  getById,
  create,
  activate,
  resetLoginCount
};
