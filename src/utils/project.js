const _ = require('lodash');
const { signUrl } = require('../services/aws/s3');

/**
 * Format models/Project
 * @arg {object} project - models/Project
 */
const format = project => {
  if (project) {
    let { developer, renderings } = project;
    // Remove developer keys
    if (developer) {
      delete project.developer_id;
      developer = _.omit(developer, ['created_at', 'updated_at']);
    }

    // Format renderings
    if (renderings) {
      if (renderings.hero_image) {
        renderings.hero_image = {
          ..._.pick(renderings.hero_image, ['id', 'name', 'type', 'metadata']),
          url: renderings.hero_image.url
            ? signUrl(renderings.hero_image.url)
            : '',
          thumbnail: renderings.hero_image.thumbnail
            ? signUrl(renderings.hero_image.thumbnail)
            : ''
        };
      }
      if (renderings.renderings?.length) {
        renderings.renderings = renderings.renderings.map(r => ({
          ..._.pick(r, ['id', 'name', 'type', 'metadata']),
          url: r.url ? signUrl(r.url) : '',
          thumbnail: r.thumbnail ? signUrl(r.thumbnail) : ''
        }));
      }
    }
  }
};

module.exports = {
  format
};
