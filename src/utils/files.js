const fs = require('fs');
const fileUpload = require('express-fileupload');

/**
 * Upload files middleware
 * @param {number} sizeLimit - size limit in MB
 * @returns
 */
const Uploader = (sizeLimit = 20) =>
  fileUpload({
    limits: { fileSize: sizeLimit * 1024 * 1024 }, // 20MB default
    abortOnLimit: true,
    useTempFiles: true,
    tempFileDir: './tmp/',
    debug: process.env.NODE_ENV === 'development'
  });

/**
 * Remove temp files uploaded by uploader
 * @param {object} files
 */
const removeTempFiles = files => {
  if (files) {
    Object.keys(files).forEach(key => {
      if (Array.isArray(files[key])) {
        for (const file of files[key]) {
          if (fs.existsSync(file.tempFilePath)) {
            fs.unlinkSync(file.tempFilePath);
          }
          if (fs.existsSync(file.thumbnail)) {
            fs.unlinkSync(file.thumbnail);
          }
        }
      } else if (fs.existsSync(files[key].tempFilePath)) {
        fs.unlinkSync(files[key].tempFilePath);
        if (fs.existsSync(files[key].thumbnail)) {
          fs.unlinkSync(files[key].thumbnail);
        }
      }
    });
  }
};

module.exports = {
  removeTempFiles,
  Uploader
};
