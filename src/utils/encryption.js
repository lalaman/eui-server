const bcrypt = require('bcryptjs');
/**
 * Hashes a string password to store in db
 * @arg {string} password
 * @returns {string} hashed password
 */
const hashPassword = password =>
  bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);

module.exports = {
  hashPassword
};
