const success = (res, data = { message: 'OK' }) => res.status(200).json(data);

/* ===============================
 * Client error codes
 * =============================== */

// Missing required fields or incorrect parameters
const badRequest = (res, message) =>
  typeof message === 'object'
    ? res.status(400).json(message)
    : res.status(400).json({ message });

// Missing correct credentials
const unauthorized = (res, message) =>
  res.status(401).json({
    message: message || 'You are not authorized to perform this action'
  });

// Custom error
const custom = (res, { statusCode, errorCode, message, data }) =>
  res.status(statusCode).json({
    statusCode,
    errorCode,
    message,
    data
  });

// Forbidden or no route found
const forbidden = res => res.status(403).json({ message: 'Forbidden' });

/* ===============================
 * Server error
 * =============================== */
const error = async (res, message, err, statusCode = 500) => {
  if (err) {
    // From validator
    if (err?.statusCode && err?.message) {
      return res.status(err.statusCode).json({ message: err.message });
    }

    if (err.response && err.response.body) console.log(err.response.body);
    else if (err.response) console.log(err.response);
    else console.log(err);
  }

  return typeof message === 'object'
    ? res.status(statusCode).json(message)
    : res.status(statusCode).json({ message });
};

module.exports = {
  success,
  badRequest,
  unauthorized,
  custom,
  forbidden,
  error
};
