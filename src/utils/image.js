const fs = require('fs');
const imageThumbnail = require('image-thumbnail');

/**
 * Generate thumbnail from image
 * @param {string|buffer} image -
 * @returns {buffer} - buffer of thumbnail
 */
const generateThumb = async image => {
  if (typeof image === 'string') image = fs.readFileSync(image);
  const options = {
    percentage: 50,
    width: 500,
    responseType: 'buffer',
    jpegOptions: { force: true, quality: 100 }
  };

  try {
    const thumbnail = await imageThumbnail(image, options);
    return thumbnail;
  } catch (err) {
    console.error(err);
    return false;
  }
};

module.exports = {
  generateThumb
};
