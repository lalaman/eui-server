const Response = require('../utils/response');

const Router = app => {
  // Initial GET route
  app.route('/').get((_, res) =>
    Response.success(res, {
      message: 'Blackline API at your service.'
    })
  );

  // Internal routes
  app.use('/account', require('./account'));
  app.use('/project', require('./project'));
  app.use('/developer', require('./developer'));

  // All other routes should send forbidden
  app.route('/*').get((_, res) => Response.forbidden(res));
  app.route('/*').post((_, res) => Response.forbidden(res));
  app.route('/*').put((_, res) => Response.forbidden(res));
  app.route('/*').patch((_, res) => Response.forbidden(res));
  app.route('/*').delete((_, res) => Response.forbidden(res));
};

module.exports = Router;
