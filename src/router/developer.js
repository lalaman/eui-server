const express = require('express');
const Auth = require('../services/auth');
const Controller = require('../controllers/developer');

const DeveloperRouter = express.Router();

DeveloperRouter.get(
  '/all',
  Auth.isAuthenticated,
  Controller.getAll
);

DeveloperRouter.post(
  '/',
  Auth.isAuthenticated,
  Controller.create
);

DeveloperRouter.put(
  '/:developer_id',
  Auth.isAuthenticated,
  Controller.update
);

DeveloperRouter.delete(
  '/:developer_id',
  Auth.isAuthenticated,
  Controller.remove
);

module.exports = DeveloperRouter;
