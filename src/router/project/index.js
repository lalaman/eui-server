const express = require('express');
const Auth = require('../../services/auth');
const { Uploader } = require('../../utils/files');
const Controller = require('../../controllers/project');

const ProjectRouter = express.Router();

ProjectRouter.get(
  '/all',
  Auth.isAuthenticated,
  Controller.getAll
);

ProjectRouter.get(
  '/:project_id',
  Auth.isAuthenticated,
  Controller.get
);

ProjectRouter.post(
  '/',
  Auth.isAuthenticated,
  Controller.create
);

ProjectRouter.put(
  '/:project_id',
  Auth.isAuthenticated,
  Controller.update
);

ProjectRouter.post(
  '/:project_id/renderings',
  Auth.isAuthenticated,
  Uploader(50),
  Controller.uploadRenderings
);

ProjectRouter.post(
  '/:project_id/renderings/remove',
  Auth.isAuthenticated,
  Controller.removeRenderings
);

module.exports = ProjectRouter;
