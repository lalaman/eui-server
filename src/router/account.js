const express = require('express');
const Controller = require('../controllers/account');

const AccountRouter = express.Router();

AccountRouter.post('/', Controller.create);
AccountRouter.post('/login', Controller.login);
AccountRouter.post('/verify', Controller.verify);

module.exports = AccountRouter;
