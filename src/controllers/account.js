const { transaction: _trx, Model } = require('objection');
const { validate } = require('../validators');
const {
  checkEmail,
  checkBodyEmail,
  checkPassword,
  checkAccountToken,
  checkConfirmPassword
} = require('../validators/account');
const { generateAccountToken } = require('../services/auth');
const {
  create: createAccount,
  activate: activateAccount,
  resetLoginCount
} = require('../services/account');
const Response = require('../utils/response');

const AccountController = {
  /**
   * Login with email/password
   * @body {string} email - email address
   * @body {string} password - unhashed password string
   * @body {boolean} keep_signed_in - determines whether generated token lasts shorter/longer
   */
  login: async (req, res) => {
    try {
      await validate(req, [checkBodyEmail(), checkPassword(req)]);

      const { keep_signed_in = false } = req.body;
      const { account } = req._v;
      const token = await generateAccountToken(account, keep_signed_in);
      await resetLoginCount(account.id);

      return Response.success(res, token);
    } catch (err) {
      const message = 'Unable to login';
      return Response.error(res, { message }, err);
    }
  },

  /**
   * Create account using email and password
   * @body {string} email
   * @body {string} password
   * @body {string} confirm_password
   * @returns {string} token
   */
  create: async (req, res) => {
    let trx;

    try {
      await validate(req, [
        checkBodyEmail(),
        checkPassword(req),
        checkConfirmPassword(req)
      ]);

      trx = await _trx.start(Model.knex());

      const { email, password } = req.body;

      const account = await createAccount({ email, password }, trx);
      const token = generateAccountToken(account);

      await trx.commit();
      trx = undefined;
      return Response.success(res, token);
    } catch (err) {
      if (trx) await trx.rollback();
      const message = 'Unable to create account';
      return Response.error(res, { message }, err);
    }
  },

  /**
   * Verify email address
   * @query {string} email - email address
   * @query {string} token - token containing account details
   * @returns {boolean} true/false
   */
  verify: async (req, res) => {
    try {
      await validate(req, [
        checkEmail(req.query.email),
        checkAccountToken(req)
      ]);

      const { account } = req._v;

      await activateAccount(account);
      return Response.success(res);
    } catch (err) {
      const message = 'Unable to verify account';
      return Response.error(res, { message }, err);
    }
  }
};

module.exports = AccountController;
