const { transaction: _trx, Model } = require('objection');
const {
  checkDeveloperBody,
  checkDeveloperId
} = require('../validators/developer');
const { validate } = require('../validators/index');
const {
  getAll: getAllDevelopers,
  getById: getDeveloperById,
  create: createDeveloper,
  update: updateDeveloper,
  remove: removeDeveloper
} = require('../services/developer');
const Response = require('../utils/response');

const DeveloperController = {
  /**
   * Get all developers
   * @returns {array} - [models/Developer]
   */
  getAll: async (req, res) => {
    const developers = await getAllDevelopers();
    return Response.success(res, developers);
  },

  /**
   * Get developer by id
   * @param {number} developer_id - id of developer
   * @returns {object} models/Developer
   */
  get: async (req, res) => {
    try {
      await checkDeveloperBody(req);
      const { developer_id } = req.params;
      const developer = await getDeveloperById(developer_id);
      return Response.success(res, developer);
    } catch (err) {
      const message = 'Unable to get developer';
      return Response.error(res, { message }, err);
    }
  },

  /**
   * Create developer
   * @body {string} name - name of developer
   * @body {object} address - { street, street_alt, city, state, country, zip}
   * @returns {object} - models/Developer
   */
  create: async (req, res) => {
    try {
      await validate(req, checkDeveloperBody(req, true));

      const { name, address } = req.body;
      const developer = await createDeveloper({ name, address });
      return Response.success(res, developer);
    } catch (err) {
      const message = 'Unable to create developer';
      return Response.error(res, { message }, err);
    }
  },
  /**
   * Update developer
   * @body {string} name - name of developer
   * @body {object} address - { street, street_alt, city, state, country, zip}
   * @returns {object} - models/Developer
   */
  update: async (req, res) => {
    let trx;

    try {
      await validate(req, checkDeveloperBody(req));
      const { name, address } = req.body;
      const { developer } = req._v;

      trx = await _trx.start(Model.knex());

      const updated = await updateDeveloper(
        developer.id,
        { name, address },
        trx
      );

      await trx.commit();
      trx = undefined;
      return Response.success(res, updated);
    } catch (err) {
      if (trx) await trx.rollback();
      const message = 'Unable to update developer';
      return Response.error(res, { message }, err);
    }
  },

  /**
   * Remove developer
   * @param {number} - developerId
   * @returns {string} - 'OK'
   */
  remove: async (req, res) => {
    let trx;

    try {
      await validate(req, [checkDeveloperId(req)]);
      const { developer } = req._v;

      trx = await _trx.start(Model.knex());

      const deleted = await removeDeveloper(developer.id, trx);

      if (deleted !== 1) throw `Deleted ${deleted} developers`;
      await trx.commit();
      trx = undefined;
      return Response.success(res);
    } catch (err) {
      if (trx) await trx.rollback();
      const message = 'Unable to delete developer';
      return Response.error(res, { message }, err);
    }
  }
};

module.exports = DeveloperController;
