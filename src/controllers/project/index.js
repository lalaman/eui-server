const { transaction: _trx, Model } = require('objection');
const {
  checkProjectBody,
  checkRenderings,
  checkRenderingIds
} = require('../../validators/project/main');
const validateProject = require('../../validators/project');
const {
  getAll: getAllProjects,
  getById: getProjectById,
  create: createProject,
  update: updateProject,
  uploadRenderings,
  getRenderings,
  removeRenderings: _removeRenderings
} = require('../../services/project');
const { format: formatProject } = require('../../utils/project');
const { removeTempFiles } = require('../../utils/files');
const Response = require('../../utils/response');

const ProjectController = {
  /**
   * Get all projects
   * @returns {array} - [models/Project]
   */
  getAll: async (req, res) => {
    const projects = await getAllProjects({
      withGraphFetched: ['developer']
    });
    return Response.success(res, projects);
  },

  /**
   * Get project by id
   * @param {number} project_id - id of project
   * @returns {object} models/Project
   */
  get: async (req, res) => {
    try {
      await validateProject(req);
      const { project_id: projectId } = req.params;
      const project = await getProjectById(projectId, {
        withGraphFetched: ['developer']
      });
      project.renderings = await getRenderings(projectId);
      formatProject(project);

      return Response.success(res, project);
    } catch (err) {
      const message = 'Unable to fetch project';
      return Response.error(res, { message }, err);
    }
  },

  /**
   * Create project
   * @body {string} name - name of project
   * @body {number} developer_id - id of project developer
   * @body {object} address - { street, street_alt, city, state, country, zip}
   * @returns {object} models/Project
   */
  create: async (req, res) => {
    let trx;
    try {
      await validateProject(req, {
        checkProjectId: false,
        checkBodyData: checkProjectBody(req, true)
      });
      const { name, address, developer_id } = req.body;

      trx = await _trx.start(Model.knex());
      const project = await createProject({ name, address, developer_id }, trx);

      await trx.commit();
      trx = undefined;

      return Response.success(res, project);
    } catch (err) {
      if (trx) await trx.rollback();
      const message = 'Unable to create project';
      return Response.error(res, { message }, err);
    }
  },

  /**
   * Update project
   * @param {number} project_id - id of project
   * @body {string} name - name of project
   * @body {number} developer_id - id of project developer
   * @body {object} address - { street, street_alt, city, state, country, zip}
   * @returns {object} models/Project
   */
  update: async (req, res) => {
    let trx;
    try {
      await validateProject(req, {
        checkBodyData: checkProjectBody(req, true)
      });
      const { name, address, developer_id } = req.body;
      const { project_id: projectId } = req.params;
      trx = await _trx.start(Model.knex());

      const project = await updateProject(
        projectId,
        { name, address, developer_id },
        trx
      );

      await trx.commit();
      trx = undefined;

      return Response.success(res, project);
    } catch (err) {
      if (trx) await trx.rollback();
      const message = 'Unable to update project';
      return Response.error(res, { message }, err);
    }
  },

  /**
   * Upload project hero image and/or renderings
   * @files {array} renderings - list of rendering files
   * @query {string} type - i.e. 'hero'
   * @returns {object} - { hero_image: {}, renderings: [] }
   */
  uploadRenderings: async (req, res) => {
    let trx;

    try {
      trx = await _trx.start(Model.knex());

      await validateProject(req, { checkBodyData: checkRenderings(req) });
      const { renderings } = req.files;
      const { project_id: projectId } = req.params;
      const type = req.query.type || 'rendering';

      await uploadRenderings(projectId, type, renderings, trx);
      await trx.commit();
      trx = undefined;

      const _renderings = formatProject({
        renderings: await getRenderings(projectId)
      });
      return Response.success(res, _renderings);
    } catch (err) {
      if (trx) await trx.rollback();
      const message = 'Unable to upload rendering(s)';
      return Response.error(res, { message }, err);
    } finally {
      removeTempFiles(req.files);
    }
  },

  /**
   * Remove project renderings
   * @param {number} project_id - id of project
   * @body {array} rendering_ids
   */
  removeRenderings: async (req, res) => {
    let trx;

    try {
      await validateProject(req, { checkBodyData: checkRenderingIds(req) });
      trx = await _trx.start(Model.knex());

      const { media } = req._v;
      const { project_id: projectId } = req.params;

      await _removeRenderings(
        projectId,
        media.map(m => m.id),
        trx
      );

      await trx.commit();
      trx = undefined;

      return Response.success(res);
    } catch (err) {
      if (trx) await trx.rollback();
      const message = 'Unable to remove rendering(s)';
      return Response.error(res, { message }, err);
    }
  }
};

module.exports = ProjectController;
