const { param } = require('express-validator');
const { handleChecks } = require('../index');
const { getById: getProjectById } = require('../../services/project');

/**
 * Core project validator
 * @param {object} req
 * @param {array} checkBodyData - optional - additional checks to add
 */
const ProjectValidator = async (
  req,
  { checkProjectId = true, checkBodyData = [] } = {}
) => {
  req._v = {};
  const checks = [];

  if (checkProjectId) {
    checks.push(...[
      // Check if project id exists
      param('project_id', 'Missing project id')
        .exists()
        .isInt()
        .withMessage('Project does not exist')
        .toInt(),

      // Check project exists
      param('project_id')
        .custom(async projectId => {
          const project = await getProjectById(projectId);
          if (!project) throw 'Project does not exist';
          req._v.project = project;
          return true;
        })
    ]);
  }

  // Additional body data checks
  checks.push(...checkBodyData);

  const error = await handleChecks(req, checks);
  if (error) throw error;
};

module.exports = ProjectValidator;
