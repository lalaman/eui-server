const { body, query } = require('express-validator');
const { getById: getDeveloperById } = require('../../services/developer');
const { getByIds: getMediaByIds } = require('../../services/media');

/**
 * Check project body
 * @param {boolean} optional - whether body is required
 */
const checkProjectBody = (req, optional = false) => {
  // Name
  const checkName = body('name', 'Name is required');
  if (optional) checkName.optional();
  else checkName.exists();
  checkName
    .isString()
    .withMessage('Invalid project name')
    .isLength({ min: 1, max: 250 })
    .withMessage('Name must be between 1 to 250 chars long')
    .trim();

  // Developer
  const checkDeveloper = body('developer_id', 'Developer is required');
  if (optional) checkDeveloper.optional();
  else checkDeveloper.exists();
  checkDeveloper
    .isInt()
    .toInt()
    .custom(async developerId => {
      const developer = await getDeveloperById(developerId);
      if (!developer) throw 'Developer does not exist';
      req._v.developer = developer;
      return true;
    });

  // Address
  const checkAddress = [
    body('address', 'Invalid address').optional().isObject(),
    ...['street', 'street_alt', 'city', 'country', 'state', 'zip'].map(prop =>
      body(`address.${prop}`).isString().trim()
    )
  ];

  return [checkName, checkDeveloper, ...checkAddress];
};

/**
 * Check uploaded renderings
 */
const checkRenderings = req => [
  query('type', 'Invalid rendering type')
    .optional()
    .isString()
    .isIn(['hero', 'rendering']),

  body().custom(() => {
    const renderings = req.files?.renderings;
    if ((Array.isArray(renderings) && !renderings.length) || !renderings) {
      throw 'Please upload at least one rendering';
    }
    if (!Array.isArray(renderings)) req.files.renderings = [renderings];
    return true;
  })
];

/**
 * Check rendering media ids
 */
const checkRenderingIds = req => [
  body('rendering_ids', 'Missing renderings to delete').exists().isArray(),
  body('rendering_ids.*', 'One or more renderings do not exist')
    .isInt()
    .toInt(),
  body('rendering_ids').custom(async ids => {
    const media = await getMediaByIds(req.params.project_id, ids);

    if (!media || media.length !== ids.length) {
      throw 'One or more renderings do not exist';
    }

    req._v.media = media;
    return true;
  })
];

module.exports = {
  checkProjectBody,
  checkRenderings,
  checkRenderingIds
};
