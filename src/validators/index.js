const { validationResult } = require('express-validator');
const { removeTempFiles } = require('../utils/files');

/**
 * Handle error messages
 * @param {array|string} err - [{ msg.[errorCode, statusCode, message] }] or 'Something went wrong'
 * @returns
 */
const handleErr = err => {
  try {
    // Regular errors
    if (typeof err.msg === 'string') {
      return { statusCode: 400, message: err.msg };
    }
    if (err[0] && typeof err[0].msg === 'string') {
      return { statusCode: 400, message: err[0].msg };
    }

    const statusCode = err[0].msg.errorCode || err[0].msg.statusCode;
    const message = err[0].msg.message || err[0].msg;

    return { statusCode, message };
  } catch (e) {
    console.log('Server error');
    console.log(e);
    return {
      statusCode: 500,
      message: 'There was an error handling your request.'
    };
  }
};

/**
 * Check all rules from express validator
 * @param {object} req - request object
 * @param {array} rules - array of express-validator checks
 * @returns {boolean|Error} - true or Error
 */
const handleChecks = async (req, rules) => {
  try {
    for (const rule of rules) {
      await rule.run(req);
      validationResult(req).throw();
    }
    return false;
  } catch (err) {
    console.log(err);
    if (
      [undefined, 'development'].includes(process.env.NODE_ENV) &&
      process.env.NODE_ENV !== 'test'
    ) {
      console.log(err.errors[0]);
    }
    return handleErr(err.array({ onlyFirstError: true }));
  }
};

/**
 * Wrapper for express-validator
 * @arg {object} req - request object
 * @arg {array} rules - express-validator styled rules
 */
const validate = async (req, rules) => {
  req._v = {};
  const error = await handleChecks(req, rules);
  if (error) {
    if (req.files) removeTempFiles(req.files);
    throw error;
  }
};

module.exports = {
  handleChecks,
  validate
};
