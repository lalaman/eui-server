const { body, param, check, query } = require('express-validator');
const { validPassword, decodeToken } = require('../services/auth');
const { getByEmail: getAccountByEmail } = require('../services/account');

const checkBodyEmail = () =>
  body('email', 'Email is invalid').exists().isEmail().toLowerCase().trim();

const checkParamEmail = () =>
  param('email', 'Email is invalid').exists().isEmail().toLowerCase().trim();

const checkEmail = email =>
  check(email, 'Email is invalid').exists().isEmail().toLowerCase().trim();

const checkPassword = req =>
  body('password', 'Password is required')
    .exists()
    .isString()
    .custom(async password => {
      const account = await getAccountByEmail(req.body.email);
      if (!account || !validPassword(password, account.password))
        throw 'Incorrect email/password';
      req._v.account = account;;
      return true;
    });

const checkConfirmPassword = req =>
  body('confirm_password', 'Passwords do not match')
    .exists()
    .isString()
    .equals(req.body.password);

const checkAccountToken = req =>
  query('token')
    .exists()
    .isString()
    .custom(async token => {
      const data = decodeToken(token);
      const account = await getAccountByEmail(req.query.email);

      if (
        !data ||
        req.query.email !== data.email ||
        !account ||
        account.id !== data.id
      )
        throw 'Invalid token provided';

      if (account.status !== 'created')
        throw 'This account has already been verified';

      req._v.account = account;
      return true;
    });

module.exports = {
  checkEmail,
  checkBodyEmail,
  checkParamEmail,
  checkPassword,
  checkAccountToken,
  checkConfirmPassword
};
