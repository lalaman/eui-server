const { body, param } = require('express-validator');
const { getById: getDeveloperById } = require('../services/developer');

const checkDeveloperId = (req, optional) => {
  // Developer
  const checkDeveloper = param('developer_id', 'Developer does not exist');

  if (optional) checkDeveloper.optional();
  else checkDeveloper.exists();

  return checkDeveloper
    .isInt()
    .toInt()
    .custom(async developerId => {
      const developer = await getDeveloperById(developerId);
      if (!developer) throw 'Developer does not exist';
      req._v.developer = developer;
      return true;
    });
};

/**
 * Check developer body
 * @param {boolean} optional - whether body is required
 */
const checkDeveloperBody = (req, optional = false) => {
  // Name
  const checkName = body('name', 'Name is required');
  if (optional) checkName.optional();
  else checkName.exists();
  checkName
    .isString()
    .withMessage('Invalid developer name')
    .isLength({ min: 1, max: 250 })
    .withMessage('Developer name must be between 1 to 250 chars long')
    .trim();

  // Address
  const checkAddress = [
    body('address', 'Invalid address').optional().isObject(),
    ...['street', 'street_alt', 'city', 'country', 'state', 'zip'].map(prop =>
      body(`address.${prop}`, `Invalid developer ${prop}`)
        .optional()
        .isString()
        .trim()
    )
  ];

  return [checkName, checkDeveloperId(req, optional), ...checkAddress];
};

module.exports = {
  checkDeveloperBody,
  checkDeveloperId
};
