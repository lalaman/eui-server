const { Model } = require('objection');

class Developer extends Model {
  static get tableName() {
    return 'developer';
  }

  $beforeUpdate() {
    this.updated_at = new Date();
  }

  static get jsonSchema() {
    return {
      type: 'object',
      required: ['name'],

      properties: {
        name: { type: 'string' },
        address: {
          type: 'object',
          properties: {
            street: { type: 'string' },
            street_alt: { type: 'string' },
            city: { type: 'string' },
            state: { type: 'string' },
            country: { type: 'string' },
            zip: { type: 'string' }
          }
        }
      }
    };
  }

  static get relationMappings() {
    const Project = require('./Project');

    return {
      projects: {
        relation: Model.HasManyRelation,
        modelClass: Project,
        join: {
          from: 'developer.id',
          to: 'project.developer_id'
        }
      }
    };
  }
}

module.exports = Developer;
