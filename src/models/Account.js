const { Model } = require('objection');

class Account extends Model {
  static get tableName() {
    return 'account';
  }

  $beforeUpdate() {
    this.updated_at = new Date();
  }

  static get jsonSchema() {
    return {
      type: 'object',
      required: ['email', 'password'],

      properties: {
        email: { type: 'string' },
        password: { type: 'string' },
        contact: { type: 'object' },
        status: { type: 'string' },
        failed_logins: { type: 'integer' }
      }
    };
  }

  static get relationMappings() {

  }
}

module.exports = Account;
