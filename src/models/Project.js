const { Model } = require('objection');

class Project extends Model {
  static get tableName() {
    return 'project';
  }

  static get jsonSchema() {
    return {
      type: 'object',
      required: ['developer_id', 'name'],

      properties: {
        developer_id: { type: ['integer', 'null'] },
        name: { type: 'string' },
        address: {
          type: 'object',
          properties: {
            street: { type: 'string' },
            street_alt: { type: 'string' },
            city: { type: 'string' },
            state: { type: 'string' },
            country: { type: 'string' },
            zip: { type: 'string' }
          }
        }
      }
    };
  }

  static get relationMappings() {
    const Developer = require('./Developer');
    const Media = require('./Media');

    return {
      developer: {
        relation: Model.BelongsToOneRelation,
        modelClass: Developer,
        join: {
          from: 'project.developer_id',
          to: 'developer.id'
        }
      },
      hero_image: {
        relation: Model.HasOneRelation,
        modelClass: Media,
        filter: query => query.where({ type: 'hero_image' }),
        join: {
          from: 'project.id',
          to: 'media.project_id'
        }
      },
      renderings: {
        relation: Model.HasManyRelation,
        modelClass: Media,
        filter: query => query.where({ type: 'rendering' }),
        join: {
          from: 'project.id',
          to: 'media.project_id'
        }
      }
    };
  }
}

module.exports = Project;
