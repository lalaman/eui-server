const { Model } = require('objection');

class Media extends Model {
  static get tableName() {
    return 'media';
  }

  $beforeUpdate() {
    this.updated_at = new Date();
  }

  static get jsonSchema() {
    return {
      type: 'object',
      required: ['entity_id', 'entity_name'],

      properties: {
        project_id: { type: 'integer' },
        entity_name: { type: 'string' },
        entity_id: { type: 'integer' },
        name: { type: 'string' },
        url: { type: 'string' },
        type: { type: 'string' },
        thumbnail: { type: 'string' },
        metadata: { type: 'object' }
      }
    };
  }

  static get relationMappings() {
    const Project = require('./Project');

    return {
      project: {
        relation: Model.BelongsToOneRelation,
        modelClass: Project,
        join: {
          from: 'media.project_id',
          to: 'project.id'
        }
      }
    };
  }
}

module.exports = Media;
