/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = async knex => {
  await knex.schema.createTable('media', table => {
    table.increments('id').primary();
    table.integer('project_id').defaultTo(null).nullable();
    table.string('entity_name').notNullable();
    table.integer('entity_id').notNullable();
    table.string('name').defaultTo('').notNullable();
    table.string('type').defaultTo('').notNullable();
    table.string('url').defaultTo('').notNullable();
    table.string('thumbnail').defaultTo('').notNullable();
    table.json('metadata')
      .defaultTo({
        caption: '',
        size: null,
        type: ''
      })
      .notNullable();
    table.timestamps(true, true);
  });
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = async knex => {
  await knex.schema.dropTable('media');
};
