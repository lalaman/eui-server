/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = async knex => {
  await knex.schema.createTable('developer', table => {
    table.increments('id').primary();
    table.string('name').unique().notNullable();
    table
      .json('address')
      .defaultTo({
        street: '',
        street_alt: '',
        city: '',
        state: '',
        country: '',
        zip: ''
      })
      .notNullable();
    table.timestamps(true, true);
  });

  await knex.schema.createTable('project', table => {
    table.increments('id').primary();

    table
      .integer('developer_id')
      .index()
      .references('id')
      .inTable('developer')
      .nullable();

    table.string('name').unique().notNullable();

    table
      .json('address')
      .defaultTo({
        street: '',
        street_alt: '',
        city: '',
        state: '',
        country: '',
        zip: ''
      })
      .notNullable();

    table.timestamps(true, true);
  });
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = async knex => {
  await knex.schema.dropTable('project');
  await knex.schema.dropTable('developer');
};
