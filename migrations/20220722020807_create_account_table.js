/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = async knex => {
  await knex.schema.createTable('account', table => {
    table.increments('id').primary();
    table.string('email').unique().notNullable();
    table.text('password').notNullable();
    table
      .json('contact')
      .defaultTo({
        full_name: '',
        phone: ''
      })
      .notNullable();
    table
      .enum('status', ['created', 'active', 'removed'])
      .defaultTo('created')
      .notNullable();
    table.integer('failed_logins').defaultTo(0).notNullable();
    table.timestamps(true, true);
  });
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = async knex => {
  await knex.schema.dropTable('account');
};
