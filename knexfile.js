require('dotenv').config();

module.exports = {
  client: 'postgres',
  connection: {
    host: process.env.DB_HOST,
    database: process.env.DB_NAME,
    user: process.env.DB_USER,
    password: process.env.DB_PASS,
    port: process.env.DB_PORT
  },
  pool: {
    min: 2,
    max: 1000,
    idleTimeoutMillis: 500
  },
  migrations: {
    tableName: 'knex_migrations'
  }
};
