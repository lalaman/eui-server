module.exports = {
  env: {
    browser: true,
    commonjs: true,
    es6: true
  },

  extends: ['prettier', 'plugin:import/recommended'],

  globals: {
    Atomics: 'readonly',
    SharedArrayBuffer: 'readonly',
    process: true,
    Buffer: true,
    __dirname: true
  },

  parserOptions: {
    ecmaVersion: 2020,
    ecmaFeatures: {
      experimentalObjectRestSpread: true
    },
    sourceType: 'module'
  },

  rules: {
    'import/no-unresolved': [2, { commonjs: true, amd: true }],
    camelcase: 0,
    'comma-dangle': 'error',
    'func-names': 0,
    'global-require': 0,
    'linebreak-style': 0,
    'new-cap': 0,
    'newline-per-chained-call': 0,
    'no-await-in-loop': 0,
    'no-console': 0,
    'no-unused-vars': 'error',
    'no-undef': 'error',
    semi: 'error',
    // indent: ['error', 2, { SwitchCase: 1 }],
    'no-else-return': [
      'error',
      {
        allowElseIf: true
      }
    ],
    quotes: ['error', 'single', { allowTemplateLiterals: true }],
    'no-param-reassign': 0,
    'no-restricted-globals': 0,
    'no-restricted-syntax': 0,
    'prefer-destructuring': 0,
    'no-underscore-dangle': 0,
    'space-before-function-paren': 0,
    'no-continue': 0,
    'arrow-parens': [1, 'as-needed', { requireForBlockBody: false }]
  }
};
